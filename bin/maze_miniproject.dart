import 'dart:async';
import 'dart:io';
import 'dart:math';

import 'package:maze_miniproject/maze_miniproject.dart' as maze_miniproject;

void main() {
  Maze mz = Maze();
  mz.showWelcome();
  mz.askNamePlayer();
  mz.askGoal();
  mz.showDescription();
  mz.showChoosingMaze(mz.askChoosingQuest());
  mz.addThings();
  mz.updateStat();
  while (true) {
    if (mz.isEnd()) {
      mz.showMessageEnd();
      if (!mz.askForNext()) break;
      mz.playAgain();
    }
    mz.askDirect();
    mz.playerWalk();
    mz.updateStat();
  }
}

class Maze {
  String _name = '';
  String _goal = '';
  late Player _py;
  int _x = 0;
  int _y = 0;
  LandMazeMap _ldMaze = LandMazeMap();
  AirMazeMap _arMaze = AirMazeMap();
  WaterMazeMap _wtMaze = WaterMazeMap();
  int _chMaze = -1;
  int _mScore = 0;
  int _nowS = 0;
  String _cDir = "";
  String _ansP = "";
  String _ans = "";

  void showWelcome() {
    print("\u{1F929} Welcome to Maze MM Game!!!");
  }

  void askNamePlayer() {
    print('\u{1F308} Please enter your name : ');
    _name = stdin.readLineSync()!;
  }

  void askGoal() {
    print('\u{1F308} Please enter you want be your goal : ');
    _goal = stdin.readLineSync()!;
  }

  void showDescription() {
    _py = Player(_name, _goal);
    print(
        '\u{1F44B} Hi! ${_py.getName()}. We so interesting your goal "${_py.getGoal()}"!');
    print('=== Maze Game ===');
    print('We have 3 Maze following for challenge you...');
    print(
        '1. Land Maze Map : Damage Barricade is Rock && Healing Wall is Blossom.');
    print(
        '2. Air Maze Map : Healing Barricade is Lady Bug && Damage Wall is Spider Web.');
    print(
        '3. Water Maze Map : Healing Barricade is Jellyfish && not only Damage Wall is Snow Flakes but have Healing Wall is Water Wave.');
    print('Hope you enjoy with us!');
  }

  int askChoosingQuest() {
    print('Please choosing your interesting maze \u{1F3AF} : ');
    _chMaze = int.parse(stdin.readLineSync()!);
    return _chMaze;
  }

  var cMaze;
  void showChoosingMaze(int chMaze) {
    switch (chMaze) {
      case 1:
        cMaze = _ldMaze;
        // _ldMaze.setMaze();
        // _ldMaze.showMaze();
        break;
      case 2:
        cMaze = _arMaze;
        // _arMaze.setMaze();
        // _arMaze.showMaze();
        break;
      case 3:
        cMaze = _wtMaze;
        // _wtMaze.setMaze();
        // _wtMaze.showMaze();
        break;
    }
    cMaze.setMaze();
    cMaze.setWall();
    print('There\'re 3 ways for you, Look at your gaol!');
    print('Ready ${cMaze.getNameMaze()}!');
    cMaze.setPlayer(_py);
  }

  void askDirect() {
    print(
        'Enter Direction : (N/w \u{2B06} , S/s \u{2B07} , W/a \u{2B05} , E/d \u{27A1} )');
    _cDir = stdin.readLineSync()!;
  }

  void playerWalk() {
    cMaze.checkDirect(_cDir);
    if (cMaze.getAskUseP()) {
      askForUse();
      cMaze.setAskUseP(false);
    }
    cMaze.showMaze();
  }

  void askForUse() {
    print('Do you want to use your score for pass it? (y/n)');
    _ansP = stdin.readLineSync()!;
    if (_ansP.compareTo('y') == 0) {
      cMaze.setAnsU(true);
      cMaze.checkDirect(_cDir);
      cMaze.setAnsU(false);
    }
  }

  bool isEnd() {
    return cMaze.getWin() ||
        (_py.getScore() + cMaze.getDeScoreWall() + cMaze.getUseP() <= 0);
  }

  int bonus() {
    if (_py.getCMovement() <= 10) {
      return 85;
    } else if (_py.getCMovement() > 10 && _py.getCMovement() <= 25) {
      return 58;
    } else if (_py.getCMovement() > 25 && _py.getCMovement() <= 40) {
      return 33;
    } else if (_py.getCMovement() > 40 && _py.getCMovement() <= 58) {
      return 11;
    }
    return 0;
  }

  int bn = 0;
  int maxScore() {
    int score = _py.getScore();
    int rd = cMaze.getDeScoreWall();
    int use = cMaze.getUseP();
    _nowS = bn + score + rd + use;
    if (_mScore < _nowS) _mScore = _nowS;
    return _mScore;
  }

  void updateStat() {
    print(
        '\u{23F3} Movement : ${_py.getCMovement() + cMaze.getCrash()} CrashWall : ${cMaze.getCrash()}(${cMaze.getDeScoreWall()}) UsePower : ${cMaze.getCount()}(${cMaze.getUseP()}) Score : ${_py.getScore() + cMaze.getDeScoreWall() + cMaze.getUseP()}');
  }

  void showMessageEnd() {
    print('--------------------------------');
    print('--------------------------------');
    print('--------------------------------');
    if (cMaze.getWin()) {
      print('\u{1F3C6} Find ${_py.getGoal()} You\'re Win!!');
      print('\u{1F308} Congratulations ${_py.getName()}!');
      if (bonus() > 0) {
        bn = bonus();
        print('\u{2764}  $bn Bonus of this Game!!');
      }
    } else {
      print('\u{1F340} Sorry with ${_py.getName()}!');
      bn = 0;
    }
    print('--------------------------------');
    print('\u{1F3AF} Best Score of All Games: ${maxScore()}');
    print(
        '\u{26F3} This Game ---------------------- \nMovement : ${_py.getCMovement() + cMaze.getCrash()} CrashWall : ${cMaze.getCrash()}(${cMaze.getDeScoreWall()}) UsePower : ${cMaze.getCount()}(${cMaze.getUseP()}) Score : $_nowS');
    print('--------------------------------');

    print('--------------------------------');
  }

  bool askForNext() {
    print('Do you want to play again? (y/n)');
    _ans = stdin.readLineSync()!;
    if (_ans.compareTo('y') == 0) {
      return true;
    }
    return false;
  }

  void playAgain() {
    _ldMaze = LandMazeMap();
    _arMaze = AirMazeMap();
    _wtMaze = WaterMazeMap();
    askGoal();
    showDescription();
    showChoosingMaze(askChoosingQuest());
    addThings();
  }

  void addThings() {
    // _ldMaze.addObj(Rock(12, 1));
    // _ldMaze.addObj(Rock(11, 16));
    // _ldMaze.addObj(Rock(15, 15));
    // _ldMaze.addObj(Rock(3, 5));
    // _ldMaze.addObj(Rock(3, 3));
    // _ldMaze.addObj(Rock(9, 14));
    // _ldMaze.addObj(Rock(8, 7));
    // _ldMaze.addObj(Rock(14, 14));

    _ldMaze.addObj([
      Rock(12, 1),
      Rock(11, 16),
      Rock(18, 8),
      Rock(3, 3),
      Rock(9, 14),
      Rock(8, 7),
      Rock(14, 14),
      Rock(3, 12),
      Rock(6, 18),
      Rock(6, 5),
      Rock(13, 5),
      Rock(14, 8),
      Rock(18, 13),
      Rock(13, 18),
      Rock(18, 16),
      Rock(5, 11)
    ]);
    _ldMaze.setObj();

    _arMaze.addObj([
      Bug(3, 1),
      Bug(16, 5),
      Bug(18, 14),
      Bug(1, 18),
      Bug(9, 14),
      Bug(11, 6),
      Bug(1, 8),
      Bug(14, 17)
    ]);
    _arMaze.setObj();

    _wtMaze.addObj([
      Jellyfish(18, 17),
      Jellyfish(1, 1),
      Jellyfish(14, 5),
      Jellyfish(3, 13),
      Jellyfish(8, 9),
    ]);
    _wtMaze.setObj();

    cMaze.showMaze();
  }
}

class Player implements Movement {
  String _name = '';
  String _goal = '';
  int _cMovement = 0;
  int _score = 39;
  int s = -1;

  @override
  String _symDirect = '';

  @override
  int _x = 0;

  @override
  int _y = 0;

  Player(this._name, this._goal);

  void setName(String name) => _name = name;
  String getName() => _name;

  void setGoal(String goal) => _goal = goal;
  String getGoal() => _goal;

  void setCMovement(int cMovement) => _cMovement = cMovement;
  int getCMovement() => _cMovement;

  void setScore(int score) => _score = score;
  int getScore() => _score;

  @override
  String getSymDirect() => _symDirect;

  @override
  int getX() => _x;

  @override
  int getY() => _y;

  @override
  void setSymDirect(String symDirect) => _symDirect = symDirect;

  @override
  void setX(int x) => _x = x;

  @override
  void setY(int y) => _y = y;

  @override
  int walkE(int x, int y) {
    _x = x + 1;
    _y = y;
    _cMovement++;
    _score += s;
    return _x;
  }

  @override
  int walkN(int x, int y) {
    _x = x;
    _y = y - 1;
    _cMovement++;
    _score += s;
    return _y;
  }

  @override
  int walkS(int x, int y) {
    _x = x;
    _y = y + 1;
    _cMovement++;
    _score += s;
    return _y;
  }

  @override
  int walkW(int x, int y) {
    _x = x - 1;
    _y = y;
    _cMovement++;
    _score += s;
    return _x;
  }
}

class Movement {
  String _symDirect;
  int _x;
  int _y;

  Movement(this._symDirect, this._x, this._y);

  void setSymDirect(String symDirect) => _symDirect = symDirect;
  String getSymDirect() => _symDirect;

  void setX(int x) => _x = x;
  int getX() => _x;

  void setY(int y) => _y = y;
  int getY() => _y;

  int walkN(int x, int y) => 0;
  int walkS(int x, int y) => 0;
  int walkE(int x, int y) => 0;
  int walkW(int x, int y) => 0;
}

abstract class MazeMap {
  String _nameMaze = "";
  final String _wallV = "||";
  final String _wallH = "===";
  late Player _py;
  late Obj _obj;
  bool _win = false;
  int _crash = 0;
  int _deScoreWall = 0;
  final int _width = 20;
  final int _height = 20;
  var _objs = <Obj>[];
  int _useP = 0;
  int _count = 0;
  bool _askUseP = false;
  bool _ansU = false;

  var _maze = List.generate(
      20, (i) => List.filled(20, " \u{25FE}", growable: false),
      growable: false);

  void setNameMaze(nameMaze) => _nameMaze = nameMaze;
  String getNameMaze() => _nameMaze;

  int getCrash() => _crash;

  int getDeScoreWall() => _deScoreWall;

  bool getWin() => _win;

  int getUseP() => _useP;

  int getCount() => _count;

  void setAskUseP(askUseP) => _askUseP = askUseP;
  bool getAskUseP() => _askUseP;

  void setAnsU(ansU) => _ansU = ansU;
  bool getAnsU() => _ansU;

  void setMaze() {
    for (int i = 0; i < _height; i++) {
      for (int j = 0; j < _width; j++) {
        if (i == 0 || i == _height - 1) {
          _maze[i][j] = _wallH;
        }
        if (j == 0 || j == _width - 1) {
          _maze[i][j] = _wallV;
        }
      }
    }
  }

  void setWall() {
    //for set different pattern in each maze
  }

  void addObj(List<Obj> obj) {
    _objs = obj;
    // _objs.add(obj);
  }

  // void addObj(Obj obj) {
  //   _obj = obj;
  //   _objs.add(obj);
  // }

  void setObj() {
    for (int i = 0; i < _objs.length; i++) {
      if (isOnMaze(_objs[i].getX(), _objs[i].getY())) {
        _maze[_objs[i].getX()][_objs[i].getY()] = _objs[i].getSymbol();
      }
    }
  }

  void setPlayer(Player py) {
    _py = py;
    var X = Random().nextInt(10) + 1; // Value is >= 1 and < 11.
    // intValue = Random().nextInt(100) + 50; // Value is >= 50 and < 150.
    var Y = Random().nextInt(10) + 1; // Value is >= 1 and < 11.
    // py.setX(2);
    // py.setY(1);
    py.setX(X);
    py.setY(Y);
    _maze[py.getY()][py.getX()] = '\u{26F3}${py.getName()[0]}';
    _maze[_height - 2][_width - 2] = '${py.getGoal()[0]}\u{1F3AF}';
    _maze[7][18] = '${py.getGoal()[0]}\u{1F3AF}';
    _maze[14][6] = '${py.getGoal()[0]}\u{1F3AF}';
  }

  void showMaze() {
    for (int i = 0; i < _height; i++) {
      for (int j = 0; j < _width; j++) {
        stdout.write('${_maze[i][j]} ');
      }
      print('');
    }
  }

  bool isWall(int x, int y) {
    if ((_maze[y][x].compareTo(_wallH) == 0 ||
        _maze[y][x].compareTo(' $_wallV') == 0)) {
      print('You\'re crash Wall!!!');
      _crash += 1;
      _deScoreWall -= 3;
      return true;
    }
    return false;
  }

  var objP;
  bool isObj(int x, int y) {
    for (int o = 0; o < _objs.length; o++) {
      if (_objs[o] is Rock &&
              _maze[y][x].compareTo(_objs[o].getSymbol()) == 0 ||
          _objs[o] is Bug && _maze[y][x].compareTo(_objs[o].getSymbol()) == 0 ||
          _objs[o] is Jellyfish &&
              _maze[y][x].compareTo(_objs[o].getSymbol()) == 0) {
        print('You\'re crash ${_objs[o].getName()}');
        objP = _objs[o];
        return true;
      }
    }
    return false;
  }

  void useObj(int x, int y) {
    int use = objP.getPower();
    _useP += use;
    _count++;
  }

  var charge;
  bool isCharge(int x, int y) {
    if (_maze[y][x].compareTo(LandMazeMap._chargeMaze) == 0) {
      print('\u{1F33C} You\'re crash healing brossom!!');
      charge = LandMazeMap._chargeMaze;
      return true;
    }
    if (_maze[y][x].compareTo(AirMazeMap._chargeMaze) == 0) {
      print('\u{1F578} You\'re crash damage spider web!!');
      charge = AirMazeMap._chargeMaze;
      return true;
    }
    if (_maze[y][x].compareTo(WaterMazeMap._chargeMazeInc) == 0) {
      print('\u{1F30A} You\'re crash healing water wave!!');
      charge = WaterMazeMap._chargeMazeInc;
      return true;
    }
    if (_maze[y][x].compareTo(WaterMazeMap._chargeMazeDec) == 0) {
      print('\u{2744} You\'re crash damage snow flake!!');
      charge = WaterMazeMap._chargeMazeDec;
      return true;
    }
    return false;
  }

  void useCharge(int x, int y) {
    int use = 0;
    if (charge == LandMazeMap._chargeMaze) {
      use = LandMazeMap.upPower();
    }
    if (charge == AirMazeMap._chargeMaze) {
      use = AirMazeMap.reducePower();
    }
    if (charge == WaterMazeMap._chargeMazeInc) {
      use = WaterMazeMap.upPower();
    }
    if (charge == WaterMazeMap._chargeMazeDec) {
      use = WaterMazeMap.reducePower();
    }
    _useP += use;
    _count++;
  }

  bool checkDirect(String symDirect) {
    int x = _py.getX();
    int y = _py.getY();
    switch (symDirect) {
      case 'N':
      case 'w':
        if (!canWalk(x, y - 1) || checkEnd(x, y - 1)) return false;
        if (isObj(x, y - 1)) {
          _askUseP = true;
          if (!_ansU) return false;
          useObj(x, y - 1);
        }
        if (isCharge(x, y - 1)) {
          _askUseP = true;
          if (!_ansU) return false;
          useCharge(x, y - 1);
        }
        // _maze[y][x] = ' . ';   //for dark mode in the future
        _maze[y][x] = ' \u{25FE}';
        y = _py.walkN(x, y);
        break;
      case 'S':
      case 's':
        if (!canWalk(x, y + 1) || checkEnd(x, y + 1)) return false;
        if (isObj(x, y + 1)) {
          _askUseP = true;
          if (!_ansU) return false;
          useObj(x, y + 1);
        }
        if (isCharge(x, y + 1)) {
          _askUseP = true;
          if (!_ansU) return false;
          useCharge(x, y + 1);
        }
        // _maze[y][x] = ' . ';
        _maze[y][x] = ' \u{25FE}';
        y = _py.walkS(x, y);
        break;
      case 'E':
      case 'd':
        if (!canWalk(x + 1, y) || checkEnd(x + 1, y)) return false;
        if (isObj(x + 1, y)) {
          _askUseP = true;
          if (!_ansU) return false;
          useObj(x + 1, y);
        }
        if (isCharge(x + 1, y)) {
          _askUseP = true;
          if (!_ansU) return false;
          useCharge(x + 1, y);
        }
        // _maze[y][x] = ' . ';
        _maze[y][x] = ' \u{25FE}';
        x = _py.walkE(x, y);
        break;
      case 'W':
      case 'a':
        if (!canWalk(x - 1, y) || checkEnd(x - 1, y)) return false;
        if (isObj(x - 1, y)) {
          _askUseP = true;
          if (!_ansU) return false;
          useObj(x - 1, y);
        }
        if (isCharge(x - 1, y)) {
          _askUseP = true;
          if (!_ansU) return false;
          useCharge(x - 1, y);
        }
        // _maze[y][x] = ' . ';
        _maze[y][x] = ' \u{25FE}';
        x = _py.walkW(x, y);
        break;
      default:
        return false;
    }
    _maze[y][x] = ' ${_py.getName()[0]} ';
    return true;
  }

  bool canWalk(int x, int y) {
    return isOnMaze(x, y) && !isWall(x, y); //&& !isObj(x, y)
  }

  bool isOnMaze(int x, int y) {
    if (!((x > 0 && x < _width - 1) && (y > 0 && y < _height - 1))) {
      return false;
    }
    return true;
  }

  bool checkEnd(int x, int y) {
    if (_maze[y][x].compareTo('${_py.getGoal()[0]}\u{1F3AF}') == 0) {
      _win = true;
      return true;
    }
    return false;
  }
}

class LandMazeMap extends MazeMap {
  @override
  String _nameMaze = "LandMaze";
  late Rock _rock;
  static String _chargeMaze = " \u{1F33C}";

  static int upPower() => 10;

  @override
  void setWall() {
    for (int i = 0; i < _height; i++) {
      for (int j = 0; j < _width; j++) {
        switch (i) {
          case 1:
            if (j == 6 || j == 12 || j == 16) {
              _maze[i][j] = ' $_wallV';
            }
            if (j == 15) _maze[i][j] = ' \u{1F33C}';
            break;
          case 2:
            if (j == 6 || j == 12) _maze[i][j] = ' $_wallV';
            if (j == 1 || j == 2 || j == 3 || j == 4 || j == 8) {
              _maze[i][j] = _wallH;
            }
            if (j == 9 || j == 10 || j == 14 || j == 15 || j == 16) {
              _maze[i][j] = _wallH;
            }
            break;
          case 3:
            if (j == 4 || j == 8 || j == 10 || j == 12 || j == 14) {
              _maze[i][j] = ' $_wallV';
            }
            if (j == 6) _maze[i][j] = ' \u{1F33C}';
            break;
          case 4:
            if (j == 17) _maze[i][j] = _wallH;
            if (j == 2 || j == 6 || j == 10 || j == 12 || j == 16) {
              _maze[i][j] = ' $_wallV';
            }
            if (j == 1) _maze[i][j] = ' \u{1F33C}';
            break;
          case 5:
            if (j == 2 || j == 3 || j == 4 || j == 6) {
              _maze[i][j] = _wallH;
            }
            if (j == 7 || j == 8 || j == 15) {
              _maze[i][j] = _wallH;
            }
            if (j == 10 || j == 12 || j == 16) {
              _maze[i][j] = ' $_wallV';
            }
            break;
          case 6:
            if (j == 18) _maze[i][j] = _wallH;
            if (j == 3 || j == 5 || j == 10 || j == 12 || j == 16) {
              _maze[i][j] = ' $_wallV';
            }
            break;
          case 7:
            if (j == 3 || j == 16) _maze[i][j] = ' $_wallV';
            if (j == 1 || j == 5 || j == 6 || j == 7 || j == 8) {
              _maze[i][j] = _wallH;
            }
            if (j == 10 || j == 12 || j == 13 || j == 14) {
              _maze[i][j] = _wallH;
            }
            if (j == 9) _maze[i][j] = ' \u{1F33C}';
            break;
          case 8:
            if (j == 17) _maze[i][j] = _wallH;
            if (j == 3 || j == 10 || j == 14 || j == 16) {
              _maze[i][j] = ' $_wallV';
            }
            break;
          case 9:
            if (j == 2 || j == 3 || j == 5 || j == 6) {
              _maze[i][j] = _wallH;
            }
            if (j == 7 || j == 8 || j == 11) {
              _maze[i][j] = _wallH;
            }
            if (j == 10) {
              _maze[i][j] = ' $_wallV';
            }
            break;
          case 10:
            if (j == 14 || j == 15 || j == 16) {
              _maze[i][j] = _wallH;
            }
            if (j == 2 || j == 5 || j == 7 || j == 10 || j == 17) {
              _maze[i][j] = ' $_wallV';
            }
            break;
          case 11:
            if (j == 4 || j == 5 || j == 9 || j == 18) {
              _maze[i][j] = _wallH;
            }
            if (j == 7 || j == 10 || j == 12 || j == 16) {
              _maze[i][j] = ' $_wallV';
            }
            break;
          case 12:
            if (j == 11 || j == 12 || j == 13) {
              _maze[i][j] = _wallH;
            }
            if (j == 7 || j == 10) {
              _maze[i][j] = ' $_wallV';
            }
            break;
          case 13:
            if (j == 2 || j == 3 || j == 5 || j == 6 || j == 7 || j == 18) {
              _maze[i][j] = _wallH;
            }
            if (j == 10 || j == 15 || j == 17) {
              _maze[i][j] = ' $_wallV';
            }
            break;
          case 14:
            if (j == 14 || j == 15) {
              _maze[i][j] = _wallH;
            }
            if (j == 3 || j == 5) {
              _maze[i][j] = ' $_wallV';
            }
            break;
          case 15:
            if (j == 1 || j == 5 || j == 6 || j == 11 || j == 12 || j == 18) {
              _maze[i][j] = _wallH;
            }
            if (j == 3 || j == 8 || j == 14) {
              _maze[i][j] = ' $_wallV';
            }
            break;
          case 16:
            if (j == 8 || j == 9 || j == 10) {
              _maze[i][j] = _wallH;
            }
            if (j == 3 || j == 6 || j == 12 || j == 17) {
              _maze[i][j] = ' $_wallV';
            }
            if (j == 2 || j == 17) _maze[i][j] = ' \u{1F33C}';
            break;
          case 17:
            if (j == 2 || j == 3 || j == 5 || j == 6 || j == 14 || j == 15) {
              _maze[i][j] = _wallH;
            }
            if (j == 17) _maze[i][j] = ' $_wallV';
            break;
          case 18:
            if (j == 2 || j == 8 || j == 11) {
              _maze[i][j] = ' $_wallV';
            }
            if (j == 9) _maze[i][j] = ' \u{1F33C}';
            break;
        }
      }
    }
  }
}

class AirMazeMap extends MazeMap {
  @override
  String _nameMaze = "AirMaze";
  late Bug _bug;
  static String _chargeMaze = " \u{1F578}";

  static int reducePower() => -5;

  @override
  void setWall() {
    for (int i = 0; i < _height; i++) {
      for (int j = 0; j < _width; j++) {
        switch (i) {
          case 1:
            if (j == 8 || j == 13 || j == 16) {
              _maze[i][j] = ' $_wallV';
            }
            if (j == 4) {
              _maze[i][j] = _wallH;
            }
            break;
          case 2:
            if (j == 2 || j == 4 || j == 10 || j == 16) {
              _maze[i][j] = ' $_wallV';
            }
            if (j == 6 || j == 7 || j == 8 || j == 11 || j == 15 || j == 18) {
              _maze[i][j] = _wallH;
            }
            break;
          case 3:
            if (j == 18) {
              _maze[i][j] = _wallH;
            }
            if (j == 2 || j == 6 || j == 11 || j == 16) {
              _maze[i][j] = ' $_wallV';
            }
            break;
          case 4:
            if (j == 2 || j == 3 || j == 5 || j == 6 || j == 8) {
              _maze[i][j] = _wallH;
            }
            if (j == 9 || j == 11 || j == 12 || j == 13 || j == 14) {
              _maze[i][j] = _wallH;
            }
            if (j == 4 || j == 18) _maze[i][j] = ' \u{1F578}';
            break;
          case 5:
            if (j == 3 || j == 8 || j == 14 || j == 17) {
              _maze[i][j] = ' $_wallV';
            }
            break;
          case 6:
            if (j == 1 || j == 2 || j == 3 || j == 4 || j == 5) {
              _maze[i][j] = _wallH;
            }
            if (j == 7 || j == 8 || j == 9 || j == 10 || j == 11) {
              _maze[i][j] = _wallH;
            }
            if (j == 12 || j == 14 || j == 16 || j == 17) {
              _maze[i][j] = _wallH;
            }
            if (j == 11) _maze[i][j] = ' \u{1F578}';
            break;
          case 7:
            if (j == 4 || j == 11 || j == 15) {
              _maze[i][j] = ' $_wallV';
            }
            break;
          case 8:
            if (j == 1 || j == 2 || j == 4 || j == 5 || j == 6 || j == 8) {
              _maze[i][j] = _wallH;
            }
            if (j == 9 || j == 12 || j == 13 || j == 14 || j == 15 || j == 16) {
              _maze[i][j] = _wallH;
            }
            if (j == 11 || j == 18) {
              _maze[i][j] = ' $_wallV';
            }
            if (j == 7) _maze[i][j] = ' \u{1F578}';
            break;
          case 9:
            if (j == 10) {
              _maze[i][j] = _wallH;
            }
            if (j == 2 || j == 9 || j == 11 || j == 13 || j == 18) {
              _maze[i][j] = ' $_wallV';
            }
            break;
          case 10:
            if (j == 2 || j == 3 || j == 4 || j == 5) {
              _maze[i][j] = _wallH;
            }
            if (j == 6 || j == 7 || j == 17 || j == 18) {
              _maze[i][j] = _wallH;
            }
            if (j == 11 || j == 16) {
              _maze[i][j] = ' $_wallV';
            }
            break;
          case 11:
            if (j == 9 || j == 18) {
              _maze[i][j] = _wallH;
            }
            if (j == 5 || j == 13 || j == 14) {
              _maze[i][j] = ' $_wallV';
            }
            break;
          case 12:
            if (j == 1 || j == 2 || j == 3 || j == 5) {
              _maze[i][j] = _wallH;
            }
            if (j == 6 || j == 7 || j == 11 || j == 15) {
              _maze[i][j] = _wallH;
            }
            if (j == 9 || j == 12 || j == 14) {
              _maze[i][j] = ' $_wallV';
            }
            break;
          case 13:
            if (j == 17) {
              _maze[i][j] = _wallH;
            }
            if (j == 5) {
              _maze[i][j] = ' $_wallV';
            }
            if (j == 9) _maze[i][j] = ' \u{1F578}';
            break;
          case 14:
            if (j == 2 || j == 3 || j == 7 || j == 8) {
              _maze[i][j] = _wallH;
            }
            if (j == 10 || j == 11 || j == 12) {
              _maze[i][j] = _wallH;
            }
            if (j == 5 || j == 15) {
              _maze[i][j] = ' $_wallV';
            }
            break;
          case 15:
            if (j == 14 || j == 15 || j == 16 || j == 18) {
              _maze[i][j] = _wallH;
            }
            if (j == 3 || j == 5 || j == 7 || j == 10 || j == 12) {
              _maze[i][j] = ' $_wallV';
            }
            if (j == 2) _maze[i][j] = ' \u{1F578}';
            break;
          case 16:
            if (j == 1 || j == 3 || j == 4 || j == 8 || j == 13) {
              _maze[i][j] = _wallH;
            }
            if (j == 7 || j == 14) {
              _maze[i][j] = ' $_wallV';
            }
            break;
          case 17:
            if (j == 1 || j == 6 || j == 10 || j == 14 || j == 15 || j == 17) {
              _maze[i][j] = _wallH;
            }
            if (j == 7 || j == 12) {
              _maze[i][j] = ' $_wallV';
            }
            if (j == 16) _maze[i][j] = ' \u{1F578}';
            break;
          case 18:
            if (j == 7) {
              _maze[i][j] = _wallH;
            }
            if (j == 3 || j == 10 || j == 15) {
              _maze[i][j] = ' $_wallV';
            }
            break;
        }
      }
    }
  }
}

class WaterMazeMap extends MazeMap {
  @override
  String _nameMaze = "WaterMaze";
  late Jellyfish _jellyfish;
  static String _chargeMazeInc = " \u{1F30A}";
  static String _chargeMazeDec = " \u{2744} ";

  static int reducePower() => -10;
  static int upPower() => 5;

  @override
  void setWall() {
    for (int i = 0; i < _height; i++) {
      for (int j = 0; j < _width; j++) {
        switch (i) {
          case 1:
            if (j == 9 || j == 15 || j == 17) {
              _maze[i][j] = ' $_wallV';
            }
            if (j == 18) _maze[i][j] = ' \u{1F30A}';
            break;
          case 2:
            if (j == 2 || j == 4 || j == 17) _maze[i][j] = ' $_wallV';
            if (j == 6 || j == 7 || j == 8 || j == 9 || j == 11 || j == 12) {
              _maze[i][j] = _wallH;
            }
            break;
          case 3:
            if (j == 1 || j == 2) {
              _maze[i][j] = _wallH;
            }
            if (j == 4 || j == 6 || j == 8 || j == 11 || j == 14) {
              _maze[i][j] = ' $_wallV';
            }
            if (j == 3) _maze[i][j] = ' \u{2744} ';
            break;
          case 4:
            if (j == 10 || j == 13 || j == 14 || j == 15 || j == 16) {
              _maze[i][j] = _wallH;
            }
            if (j == 4 || j == 8 || j == 11) {
              _maze[i][j] = ' $_wallV';
            }
            if (j == 9) _maze[i][j] = ' \u{2744} ';
            break;
          case 5:
            if (j == 1 || j == 3 || j == 4 || j == 7 || j == 8 || j == 18) {
              _maze[i][j] = _wallH;
            }
            if (j == 11) {
              _maze[i][j] = ' $_wallV';
            }
            if (j == 16) _maze[i][j] = ' \u{2744} ';
            break;
          case 6:
            if (j == 4 || j == 11 || j == 13 || j == 15) {
              _maze[i][j] = ' $_wallV';
            }
            if (j == 5) _maze[i][j] = ' \u{1F30A}';
            break;
          case 7:
            if (j == 1 || j == 2 || j == 4 || j == 5 || j == 7 || j == 8) {
              _maze[i][j] = _wallH;
            }
            if (j == 9 || j == 10 || j == 11 || j == 12) {
              _maze[i][j] = _wallH;
            }
            if (j == 13 || j == 16 || j == 17) {
              _maze[i][j] = _wallH;
            }
            if (j == 15) {
              _maze[i][j] = ' $_wallV';
            }
            break;
          case 8:
            if (j == 2 || j == 5 || j == 8 || j == 15 || j == 17) {
              _maze[i][j] = ' $_wallV';
            }
            break;
          case 9:
            if (j == 2 || j == 3 || j == 4 || j == 5 || j == 6 || j == 10) {
              _maze[i][j] = _wallH;
            }
            if (j == 11 || j == 12 || j == 13 || j == 14 || j == 15) {
              _maze[i][j] = _wallH;
            }
            if (j == 8) {
              _maze[i][j] = ' $_wallV';
            }
            if (j == 18) _maze[i][j] = ' \u{2744} ';
            break;
          case 10:
            if (j == 18) {
              _maze[i][j] = _wallH;
            }
            if (j == 6 || j == 8 || j == 13) {
              _maze[i][j] = ' $_wallV';
            }
            if (j == 7) _maze[i][j] = ' \u{2744} ';
            break;
          case 11:
            if (j == 1 || j == 2 || j == 3 || j == 9) {
              _maze[i][j] = _wallH;
            }
            if (j == 10 || j == 11 || j == 15 || j == 16) {
              _maze[i][j] = _wallH;
            }
            if (j == 8) {
              _maze[i][j] = ' $_wallV';
            }
            if (j == 1) _maze[i][j] = ' \u{2744} ';
            break;
          case 12:
            if (j == 18) {
              _maze[i][j] = _wallH;
            }
            if (j == 2 || j == 5 || j == 8 || j == 11 || j == 15) {
              _maze[i][j] = ' $_wallV';
            }
            if (j == 15) _maze[i][j] = ' \u{1F30A}';
            break;
          case 13:
            if (j == 4 || j == 5 || j == 6 || j == 7 || j == 8 || j == 10) {
              _maze[i][j] = _wallH;
            }
            if (j == 11 || j == 12 || j == 13 || j == 14 || j == 15) {
              _maze[i][j] = _wallH;
            }
            break;
          case 14:
            if (j == 2 || j == 3) {
              _maze[i][j] = _wallH;
            }
            if (j == 4 || j == 7 || j == 13 || j == 17) {
              _maze[i][j] = ' $_wallV';
            }
            break;
          case 15:
            if (j == 6 || j == 7 || j == 8 || j == 9 || j == 13 || j == 14) {
              _maze[i][j] = _wallH;
            }
            if (j == 4 || j == 17) {
              _maze[i][j] = ' $_wallV';
            }
            if (j == 9) _maze[i][j] = ' \u{1F30A}';
            if (j == 5 || j == 15) _maze[i][j] = ' \u{2744} ';
            break;
          case 16:
            if (j == 16 || j == 17 || j == 18) {
              _maze[i][j] = _wallH;
            }
            if (j == 2 || j == 9 || j == 11) {
              _maze[i][j] = ' $_wallV';
            }
            break;
          case 17:
            if (j == 3 || j == 6 || j == 7 || j == 13 || j == 14) {
              _maze[i][j] = _wallH;
            }
            if (j == 5 || j == 9 || j == 11 || j == 16) {
              _maze[i][j] = ' $_wallV';
            }
            if (j == 1) _maze[i][j] = ' \u{1F30A}';
            break;
          case 18:
            if (j == 1 || j == 5 || j == 11) {
              _maze[i][j] = ' $_wallV';
            }
            if (j == 16) _maze[i][j] = ' \u{2744} ';
            break;
        }
      }
    }
  }
}

abstract class Obj {
  String _name = "";
  String _symbol = "";
  int _x;
  int _y;
  int _power = 0;

  Obj(this._x, this._y);

  void setName(String name) => _name = name;
  String getName() => _name;

  void setSymbol(String symbol) => _symbol = symbol;
  String getSymbol() => _symbol;

  void setX(int x) => _x = x;
  int getX() => _x;

  void setY(int y) => _y = y;
  int getY() => _y;

  void setPower(int power) => _power = power;
  int getPower() => _power;
}

class Rock extends Obj {
  @override
  String _name = "Rock";
  @override
  // String _symbol = "Rk ";
  String _symbol = "\u{1F9F1}";
  @override
  int _power = -5;
  Rock(int x, int y) : super(x, y);
}

class Bug extends Obj {
  @override
  String _name = "Lady Bug";
  @override
  // String _symbol = "Bg ";
  String _symbol = " \u{1F41E}";
  @override
  int _power = 5;
  Bug(int x, int y) : super(x, y);
}

class Jellyfish extends Obj {
  @override
  String _name = "Jellyfish";
  @override
  // String _symbol = "Jf ";
  String _symbol = " \u{1F419}";
  @override
  int _power = 10;
  Jellyfish(int x, int y) : super(x, y);
}
